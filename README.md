# Typingtrainer
--------------------
Typingtrainer is a TUI program used to learn other keyboard layouts. <br>
There are a total of six difficulties for each layout, as well as individual words or full text.

- Colemak
- Colemak-DH
- Dvorak
- Dvorak-left
- Dvorak-right
- Tarmak 1-4


Requirements
------------
* ncurses

Recommended
------------
* ttf-nerd-fonts-symbols (used in `-h *layout*`)


Installation
------------

    make
    make install


Run
------------
    ttrainer -flags
