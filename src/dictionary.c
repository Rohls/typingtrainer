#include "dictionary.h"

int iskey(const char* keys, int c)
{
    int len = strlen(keys);
    for (int i = 0; i < len; i++) {
        if (keys[i] == c) return 1;
    }
    return 0;
}

dictionary* dict_init(void)
{
    dictionary* d = (dictionary*)malloc(sizeof(dictionary));
    if (!d) die("dict alloc");
    d->cap = 2000000;
    d->len = 0;
    char** ptr = (char**)malloc(sizeof(char*) * d->cap);
    if (!ptr) die("words alloc");
    d->data = ptr;
    return d;
}

void pushstr(dictionary* d, char* s, size_t len)
{
    /*if (!s) return;*/
    char* word = (char*)malloc(sizeof(char) * len + 1);
    memcpy(word, s, len);
    word[len] = '\0';
    d->data[d->len] = word;
    ++d->len;
}

void dict_free(dictionary* d)
{
    if (!d->data) return;
    for (int i = 0; i < d->len; i++) {
        if (d->data[i]) free(d->data[i]);
    }
}

void dict_randomize(dictionary* dict, settings* s)
{
    if (dict->len == 0) die("dict_randomize(): dictionary len = 0");
    if (dict->len < s->numwords) s->numwords = dict->len;
    int indices[s->numwords];

    srand(time(NULL));
    for (int i = 0; i < s->numwords; i++) {
        indices[i] = rand() % dict->len;
    }

    char** rwords = (char**)malloc(sizeof(char*) * s->numwords);
    if (!rwords) die("dict_randomize(): rwords alloc failed");
    char* word = NULL;
    size_t len = 0;
    for (int i = 0; i < s->numwords; i++) {

        if (dict->data[indices[i]]) {
            len = strlen(dict->data[indices[i]]) + 1;
            word = (char*)malloc(sizeof(char) + len);
            if (!word) die("dict_randomize(): word alloc failed");
            memcpy(word, dict->data[indices[i]], len);
            word[len] = '\0';
            rwords[i] = word;
        }
    }

    for (int i = 0; i < dict->len; i++) {
        free(dict->data[i]);
    }

    free(dict->data);
    dict->data = rwords;
    dict->len = s->numwords;
}

int romeojuliet(dictionary* dict, settings* s)
{
    FILE* f = fopen("romeo_and_juliet.txt", "r");
    if (!f) {
        f = fopen("/usr/local/share/ttrainer/romeo_and_juliet.txt", "r");
        if (!f) die("Cannot open file: romeo_and_juliet");
    }

    int c;
    char word[64];
    int wi = 0;
    int toggle = 0;
    while ((c = fgetc(f)) != EOF) {
        if (toggle) {
            if (isalpha(c) || ispunct(c)) {
                pushstr(dict, word, wi);
                memset(word, 0, 64);
                wi = 0;
                word[wi++] = c;
                toggle = 0;
            }

        } else {

            if (c == ' ' || c == '\n') {
                toggle = 1;
            } else {
                if (isalpha(c) || ispunct(c)) word[wi++] = c;
            }
        }
    }

    if (s->entirefile) s->numwords = dict->len;
    fclose(f);
    return 1;
}

int readfile(dictionary* dict, settings* s, const char* fpath, const char* keys)
{
    if (!fpath) fpath = "engwords.txt";
    FILE* f = fopen(fpath, "r");
    if (!f) {
        f = fopen("/usr/local/share/ttrainer/engwords.txt", "r");
        if (!f) die("Cannot open file: engwords");
    }

    int c;
    char word[64];
    int wi = 0;
    while ((c = fgetc(f)) != EOF) {
        if (c != '\n') {
            if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) word[wi++] = c;
        } else {
            int r = 1;
            if (keys) {
                for (int i = 0; i < wi; i++) {
                    if (!iskey(keys, word[i])) {
                        r = 0;
                    }
                }
            }

            if (r) {
                pushstr(dict, word, wi);
            }

            memset(word, 0, wi);
            wi = 0;
        }
    }

    if (s->entirefile) s->numwords = dict->len;
    fclose(f);
    return 1;
}


void dict_readfile(dictionary* d, settings* s, data* data, const char* fpath)
{
    if (s->textfile) {
        romeojuliet(d, s);
        if (s->random) {
                srand(time(NULL));
                int index = rand() % d->len;
                if (index + s->numwords > d->len) index -= s->numwords;
                if (index < 0) index = 0;
                else if (index < s->numwords) index += s->numwords;

                data->curword = index;
                s->numwords += index;

        }
    } else {
        char* keys = getlayout(s->layout, s->level, 0);
        readfile(d, s, NULL, keys);

        dict_randomize(d, s);

        free(keys);
    }
}
