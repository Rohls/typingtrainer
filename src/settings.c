#include "settings.h"

settings* settings_init(void)
{
    settings* s = (settings*)malloc(sizeof(settings));
    s->layout = 0;
    s->level = 0;
    s->numwords = 50;
    s->random = 0;
    s->kb = 0;
    s->kb_intr = 0;
    s->sentencelen = 5;
    s->sentenceheight = 4;
    s->entirefile = 0;
    s->interactive = 0;
    s->textfile = 0;
    return s;
}

int setlayout(settings* s, char* str)
{
    if      (strcmp("c", str) == 0)          return COLEMAK;
    else if (strcmp("cdh", str) == 0)        return COLEMAK_DH;
    else if (strcmp("d", str) == 0)          return DVORAK;
    else if (strcmp("dl", str) == 0)         return DVORAK_L;
    else if (strcmp("dr", str) == 0)         return DVORAK_R;

    else if (strcmp("t", str) == 0)          return TARMAK_1;
    else if (strcmp("t1", str) == 0)         return TARMAK_1;
    else if (strcmp("t2", str) == 0)         return TARMAK_2;
    else if (strcmp("t3", str) == 0)         return TARMAK_3;
    else if (strcmp("t4", str) == 0)         return TARMAK_4;

    else if (strcmp("colemak", str) == 0)    return COLEMAK;
    else if (strcmp("colemak-dh", str) == 0) return COLEMAK_DH;
    else if (strcmp("dvorak", str) == 0)     return DVORAK;
    else if (strcmp("dvorak-l", str) == 0)   return DVORAK_L;
    else if (strcmp("dvorak-r", str) == 0)   return DVORAK_R;

    else if (strcmp("tarmak", str) == 0)     return TARMAK_1;
    else if (strcmp("tarmak-1", str) == 0)   return TARMAK_1;
    else if (strcmp("tarmak-2", str) == 0)   return TARMAK_2;
    else if (strcmp("tarmak-3", str) == 0)   return TARMAK_3;
    else if (strcmp("tarmak-4", str) == 0)   return TARMAK_4;

    return 0;
}

int isnum(char* s)
{
    int len = strlen(s);
    int i = (s[0] == '-') ? 1 : 0;
    for (i = i; i < len; i++) {
        if (s[i] < 48 || s[i] > 57) return 0;
    }
    return 1;
}

// FML
int is_flag(char* s)
{
    if (s[0] == '-' && isalpha(s[1])) return 1;
    else if (s[0] == '-' && s[1] == '-' && isalpha(s[2])) return 2;
    else return 0;
}
/*(argv[i][0] == '-' && isalpha(argv[i][1]))*/

int parse_parameters(int argc, char** argv, settings* s)
{
    if (argc == 1 || !s) return 1;
    int res = 0;
    for (int i = 1; i < argc; i++) {
        res = is_flag(argv[i]);
        if (!res) continue;
        switch (argv[i][res]) {
            case 'h':
                // help
                if (i < argc - 1) {
                    parse_help_params(argv[i+1]);
                    return 0;
                }
                o_help();
                return 0;
            case 'v':
                o_version();
                return 0;
                // version
            case 'k':
                s->kb = 1;
                break;
            case 'n':
                // word cap
                if (i < argc - 1) {
                    if (isnum(argv[i+1])) {
                        int x = atoi(argv[i+1]);
                        if (x < 0) x = -x;
                        if (x != 0) // If x is 0, keep default value.
                            s->numwords = x;
                    }
                }
                break;
            case 'i':
                s->kb = 1;
                s->interactive = 1;
                break;
            case 'r':
                s->random = 1;
                break;
            case 'l':
                // layout
                if (i < argc - 1) {
                    s->layout = setlayout(s, argv[i+1]);
                }
                break;
            case 'd':
                // level(difficulty)
                if (i < argc - 1) {
                    if (isnum(argv[i+1])) {
                        int x = atoi(argv[i+1]);
                        s->level = x;
                    }
                }
                break;
            case 'a':
                // entire file
                s->entirefile = 1;
                break;
            case 't':
                s->textfile = 1;
                s->sentencelen = 7;
                break;
            default:
                break;
        }
    }
    return 1;
}
