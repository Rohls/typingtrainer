#include "timer.h"

timer* timer_init(void)
{
    timer* t = (timer*)malloc(sizeof(timer));
    if (!t) die("Cannot allocate timer");
    t->hours = 0;
    t->minutes = 0;
    t->seconds = 0;
    t->time = 0;
    return t;
}

void timer_start(timer* t)
{
    t->start = time(NULL);
}

void timer_stop(timer* t)
{
    t->finish = time(NULL);
}

void timer_calc_time(timer* t)
{
    time_t res = t->finish - t->start;
    t->time = res;
    t->hours = res / 3600;
    res %= 3600;
    t->minutes = res / 60;
    res %= 60;
    t->seconds = res;
}
