#include "io.h"

void toggleattr(WINDOW* win, int color)
{
    if (win) {
        if      (color == 0)wattroff(win, COLOR_PAIR(1) | COLOR_PAIR(2) | COLOR_PAIR(3) | COLOR_PAIR(4) | COLOR_PAIR(5) | COLOR_PAIR(6) | A_BOLD);
        else if (color == 1) wattron(win, COLOR_PAIR(1) | A_BOLD);
        else if (color == 2) wattron(win, COLOR_PAIR(2) | A_BOLD);
        else if (color == 3) wattron(win, COLOR_PAIR(3) | A_BOLD);
        else if (color == 4) wattron(win, COLOR_PAIR(4) | A_BOLD);
        else if (color == 5) wattron(win, COLOR_PAIR(5) | A_BOLD);
        else if (color == 6) wattron(win, COLOR_PAIR(6) | A_BOLD);
    } else {
        if      (color == 0)attroff(COLOR_PAIR(1) | COLOR_PAIR(2) | COLOR_PAIR(3) | COLOR_PAIR(4) | COLOR_PAIR(5) | COLOR_PAIR(6) | A_BOLD);
        else if (color == 1) attron(COLOR_PAIR(1) | A_BOLD);
        else if (color == 2) attron(COLOR_PAIR(2) | A_BOLD);
        else if (color == 3) attron(COLOR_PAIR(3) | A_BOLD);
        else if (color == 4) attron(COLOR_PAIR(4) | A_BOLD);
        else if (color == 5) attron(COLOR_PAIR(5) | A_BOLD);
        else if (color == 6) attron(COLOR_PAIR(6) | A_BOLD);
    }
}

void charadd(WINDOW* win, int y, int x, char c, int attrcolor)
{
    toggleattr(win, attrcolor);
    mvwaddch(win, y, x, c);
    if (attrcolor > 0) toggleattr(win, 0);
}
int is_useless(int c)
{
    if (c == KEY_UP || c == KEY_DOWN || c == KEY_LEFT || c == KEY_RIGHT) return 1;
    if (c == 27 || c == 13 || c == 9 || c == KEY_DC || c == KEY_SDC) return 1;
    if ( c == ('d' & 0x1F)) return 1;
    if (c == KEY_BTAB || c == KEY_CTAB || c == KEY_STAB || c == KEY_CATAB) return 1;
    return 0;
}

void read_input(dictionary* dict, settings* s, data* d, display* disp, display* kbd, keyboard* kb, timer* t)
{
    WINDOW* win = disp->win;
    int c = 0;
    int idx = 0;
    int minidx = 0;
    int offset = disp->offset;
    int maxw = disp->winw - disp->offset - 2;
    int fuckup_idx = 0;
    int proceed = 1;
    int counter = 0;
    int quitcnt = 0;
    Vector sentence = VEC_INIT;
    Vector input = VEC_INIT;

    static int timer_started = 0;

    char* word = dict->data[d->curword];
    if (!word) die("read_input(): initial word* = NULL");
    int wordlen = strlen(word);

    int max = d->curword + s->sentencelen;
    if (max >= s->numwords) max = s->numwords;
    if (d->curword >= s->numwords) return;
    for (int i = d->curword; i < max; i++) {
        vec_push_str(&sentence, dict->data[i], strlen(dict->data[i]));
        vec_push_c(&sentence, ' ');
    }


    while (1) {
        if (s->interactive && proceed) {
            if (idx < wordlen)
                kb_highlight_key(kb, kbd, word[idx]);

        }

        c = wgetch(win);
        if (!timer_started) {
            timer_start(t);
            timer_started = 1;
        }


        if (c == ('f' & 0x1F)) {
            ++quitcnt;
            if (quitcnt == QUIT_TIMES) {
                d->curword = s->numwords;
                timer_stop(t);
                return;

            }
            continue;
        } else {
            quitcnt = 0;
        }

        if (s->layout > 0) {
            if      (s->layout == COLEMAK)      c = convert_colemak(c);
            else if (s->layout == COLEMAK_DH)   c = convert_colemak_dh(c);
            else if (s->layout == DVORAK)       c = convert_dvorak(c);
            else if (s->layout == DVORAK_L)     c = convert_dvorak_l(c);
            else if (s->layout == DVORAK_R)     c = convert_dvorak_r(c);
            else if (s->layout == TARMAK_1)     c = convert_tarmak_1(c);
            else if (s->layout == TARMAK_2)     c = convert_tarmak_2(c);
            else if (s->layout == TARMAK_3)     c = convert_tarmak_3(c);
            else if (s->layout == TARMAK_4)     c = convert_tarmak_4(c);
        }

        if (is_useless(c)) {
            // kek
        } else if (c == KEY_BACKSPACE) {
            if (idx >= 0) {
                if (idx+minidx <= sentence.len) {

                    if (idx == 0)
                        charadd(win, 1, offset+minidx, sentence.c[idx+minidx], CLEAR);
                    else {
                        --idx;
                        charadd(win, 1, idx+offset+minidx, sentence.c[idx+minidx], CLEAR);
                    }
                } else if (idx+minidx <= maxw+1) {
                    charadd(win, 1, idx+minidx, ' ', CLEAR);
                    --idx;
                }
                if (!proceed && fuckup_idx == idx+minidx) proceed = 1;
                vec_pop_c(&input);
            }
        } else {
            if (idx+minidx <= maxw) {


                if (c == ' ') {
                    if (input.len == wordlen) {
                        if (vec_str_equal(&input, word)) {
                            if (d->curword == s->numwords-1) {
                                ++d->wordcompl;
                                ++d->curword;
                                timer_stop(t);
                                return;
                            }
                            ++d->curword;
                            minidx += wordlen + 1;
                            idx = 0;
                            proceed = 1;
                            fuckup_idx = 0;
                            word = dict->data[d->curword];
                            wordlen = strlen(word);
                            ++d->wordcompl;
                            if (counter == s->sentencelen-1) return;
                            ++counter;
                            vec_reset(&input);
                            continue;
                        }
                    }
                }

                ++d->kpresses;
                if (idx+minidx < sentence.len) {
                    // If input matches current letter.
                    if ( proceed && c == word[idx]) {
                        charadd(win, 1, offset+idx+minidx, word[idx], CORRECT);
                        if (s->interactive && idx == wordlen-1) kb_clear_hl(kb, kbd);
                    } else {
                        ++d->incor;
                        charadd(win, 1, offset+idx+minidx, sentence.c[idx+minidx], WRONG);
                        if (proceed) fuckup_idx = idx+minidx;
                        proceed = 0;
                    }
                } else {
                    ++d->incor;
                    charadd(win, 1, offset+idx+minidx, ' ', WRONG);
                    if (proceed) fuckup_idx = idx+minidx;
                    proceed = 0;
                }
                ++idx;
                vec_push_c(&input, c);
            }
        }

    }


    vec_reset(&input);
    vec_reset(&sentence);
}

void update_text(dictionary* dict, settings* s, display* disp, data* d)
{
    WINDOW* win = disp->win;
    if (d->curword == s->numwords) return;
    wclear(win);
    box(win, 0, 0);
    int start = d->curword;
    int end = (start + s->sentencelen >= s->numwords) ? s->numwords : start + s->sentencelen;

    for (int i = 0; i < s->sentenceheight; i++) {
        if (start >= s->numwords) return;
        if (end > s->numwords) end = s->numwords;
        wmove(win, i+1, 1);
        for (int j = start; j < end; j++) {
            waddstr(win, dict->data[j]);
            waddch(win, ' ');
        }
        start += s->sentencelen;
        end += s->sentencelen;
    }
    wrefresh(win);
}

void o_help(void)
{
    printf("usage:\n  ttrainer [options]\n\n"
           "general options:\n"
           "  -v\t\t  program version\n"
           "  -h\t\t  help [flags]\n\n"
           "ttrainer options:\n"
           "  -a\t\t  set word limit to file length\n"
           "  -l\t\t  select keyboard layout (assumes US qwerty for conversion)\n"
           "  -d\t\t  set difficulty: 1-6 (default: 6)\n"
           "  -n\t\t  set word limit\n"
           "  -t\t\t  set full sentences\n"
           "  -r\t\t  set random start\n"
           "  -k\t\t  show keyboard layout\n"
           "  -i\t\t  show interactive keyboard\n\n"
           "Tired of using the inefficient and archaic QWERTY?\n"
           "Practice with another layout on single words or full text.\n\n"
           "Press ctrl+f twice to quit early.\n\n");
}

// Colemak {{{

void colemak_layout(void)
{
    printf("\nColemak\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ F  │ P  │ G  │ L  │ J  │ U  │ Y  │ ;  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ R  │ S  ║ T  ║ D  │ H  ║ N  ║ E  │ I  │ O  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ Z  │ X  │ C  │ V  │ B  │ K  │ M  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n");
}

void colemak_level(void)
{
    printf("\nDifficulties:\n"
           "                                                                   \n"
           "      1.                     2.                  \n"
           "                                                                   \n"
           "      3.                   4.                        \n"
           "                                                     \n"
           "                                                               \n"
           "                                                                   \n"
           "      5.                     6.                   \n"
           "                                               \n"
           "                                                       \n"
           "                                                                   \n");
}

void o_help_colemak(void)
{
    colemak_layout();
    colemak_level();
}

// }}}

// Colemak DH {{{

void colemak_dh_layout(void)
{
    printf("\nColemak-DH mod:\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ F  │ P  │ B  │ L  │ J  │ U  │ Y  │ ;  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ R  │ S  ║ T  ║ G  │ M  ║ N  ║ E  │ I  │ O  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ X  │ C  │ D  │ V  │ Z  │ K  │ H  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n");
}

void colemak_dh_level(void)
{
    printf("\nDifficulties:\n"
           "                                                                   \n"
           "      1.                     2.                  \n"
           "                                                                   \n"
           "      3.                   4.                        \n"
           "                                                     \n"
           "                                                               \n"
           "                                                                   \n"
           "      5.                     6.                   \n"
           "                                               \n"
           "                                                       \n"
           "                                                                   \n");
}

void o_help_colemak_dh(void)
{
    colemak_dh_layout();
    colemak_dh_level();
}

// }}}

// Dvorak {{{

void dvorak_layout(void)
{
    printf("\nDvorak\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ '  │ ,  │ .  │ P  │ Y  │ F  │ G  │ C  │ R  │ L  │ /  │ =  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ O  │ E  ║ U  ║ I  │ D  ║ H  ║ T  │ N  │ S  │ -  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ ;  │ Q  │ J  │ K  │ X  │ B  │ M  │ W  │ V  │ Z  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n");
}

void dvorak_level(void)
{
    printf("\nDifficulties:\n"
           "                                                                   \n"
           "      1.                     2.                  \n"
           "                                                                   \n"
           "      3.                   4.                        \n"
           "                                                     \n"
           "                                                               \n"
           "                                                                   \n"
           "      5.                      6.                     \n"
           "                                               \n"
           "                                                     \n"
           "                                                                   \n");
}

void o_help_dvorak(void)
{
    dvorak_layout();
    dvorak_level();
}

// }}}

// Dvorak Left {{{

void dvorak_l_layout(void)
{
    printf("Dvorak left-handed\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ [  │ ]  │ /  │ P  │ F  │ M  │ L  │ J  │ 4  │ 3  │ 2  │ 1  │\n"
           "└──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ ;  │ Q  │ B  │ Y  │ U  │ R  │ S  │ O  │ .  │ 6  │ 5  │ =  │\n"
           "   └──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "      │ -  │ K  │ C  ║ D  ║ T  │ H  ║ E  ║ A  │ Z  │ 8  │ 7  │ \\  │\n"
           "      └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "         │ '  │ X  │ G  │ V  │ W  │ N  │ I  │ ,  │ 0  │ 9  │\n"
           "         └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n");
}

void dvorak_l_level(void)
{
    printf("\nDifficulties:\n"
           "                                                       \n"
           "      1.                2.                 \n"
           "                                                    \n"
           "                                                       \n"
           "      3.                   4.                  \n"
           "                                           \n"
           "                                               \n"
           "                                                       \n"
           "      5.                 6.                  \n"
           "                                           \n"
           "                                         \n"
           "                                            \n"
           "                                                       \n");
}

void o_help_dvorak_l(void)
{
    dvorak_l_layout();
    dvorak_l_level();
}

// }}}

// Dvorak Right {{{

void dvorak_r_layout(void)
{
    printf("\nDvorak right-handed\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ 1  │ 2  │ 3  │ 4  │ J  │ L  │ M  │ F  │ P  │ /  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ 5  │ 6  │ Q  │ .  │ O  │ R  │ S  │ U  │ Y  │ B  │ ;  │ =  │\n"
           "   └──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "      │ 7  │ 8  │ Z  ║ A  ║ E  │ T  ║ H  ║ D  │ C  │ K  │ -  │ \\  │\n"
           "      └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "         │ 9  │ 0  │ X  │ ,  │ I  │ N  │ W  │ V  │ G  │ '  │\n"
           "         └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n");
}

void dvorak_r_level(void)
{
    printf("\nDifficulties:\n"
           "                                                       \n"
           "      1.                2.                 \n"
           "                                                    \n"
           "                                                       \n"
           "      3.                   4.                  \n"
           "                                           \n"
           "                                               \n"
           "                                                       \n"
           "      5.                 6.                  \n"
           "                                           \n"
           "                                         \n"
           "                                           \n"
           "                                                       \n");
}

void o_help_dvorak_r(void)
{
    dvorak_r_layout();
    dvorak_r_level();
}

// }}}

// Tarmak 1 {{{

void o_help_tarmak_1(void)
{
    printf("\nTarmak 1\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ J  │ R  │ T  │ Y  │ U  │ I  │ O  │ P  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ S  │ D  ║ F  ║ G  │ H  ║ N  ║ E  │ L  │ ;  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ Z  │ X  │ C  │ V  │ B  │ K  │ M  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n\n");
}

// }}}

// Tarmak 2 {{{

void o_help_tarmak_2(void)
{
    printf("\nTarmak 2\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ F  │ R  │ G  │ Y  │ U  │ I  │ O  │ P  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ S  │ D  ║ T  ║ J  │ H  ║ N  ║ E  │ L  │ ;  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ Z  │ X  │ C  │ V  │ B  │ K  │ M  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n\n");
}

// }}}

// Tarmak 3 {{{

void o_help_tarmak_3(void)
{
    printf("\nTarmak 3\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ F  │ J  │ G  │ Y  │ U  │ I  │ O  │ P  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ R  │ S  ║ T  ║ D  │ H  ║ N  ║ E  │ L  │ ;  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ Z  │ X  │ C  │ V  │ B  │ K  │ M  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n\n");
}

// }}}

// Tarmak 4 {{{

void o_help_tarmak_4(void)
{
    printf("\nTarmak 4\n"
           "┌────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐\n"
           "│ Q  │ W  │ F  │ P  │ G  │ J  │ U  │ I  │ Y  │ ;  │ [  │ ]  │\n"
           "└──┬─┴──┬─┴──┬─┴──╔═╧══╗─┴──┬─┴──╔═╧══╗─┴──┬─┴──┬─┴──┬─┴──┬─┴──┐\n"
           "   │ A  │ R  │ S  ║ T  ║ D  │ H  ║ N  ║ E  │ L  │ O  │ '  │ \\  │\n"
           "   └──┬─┴──┬─┴──┬─╚══╤═╝──┬─┴──┬─╚══╤═╝──┬─┴──┬─┴──┬─┴──┬─┴────┘\n"
           "      │ Z  │ X  │ C  │ V  │ B  │ K  │ M  │ ,  │ .  │ /  │\n"
           "      └────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘\n\n");
}

// }}}

void o_help_layouts(void)
{
    printf("\nThe program assumes the user layout is US qwerty.\n\n"
            "usage:\n"
            "  -l [layout]\n\n"
            "layouts:\n"
            "  colemak,      c\n"
            "  colemak-dh,   cdh\n"
            "  dvorak,       d\n"
            "  dvorak-l,     dl\n"
            "  dvorak-r,     dr\n"
            "  tarmak-1,     t, t1\n"
            "  tarmak-2,     t2\n"
            "  tarmak-3,     t3\n"
            "  tarmak-4,     t4\n\n"
            "If incorrect, the program will default to US qwerty.\n\n");
}

void o_help_difficulty(void)
{
    printf("\nUse -h *layout* for layout specific difficulties\n\n");
}


void parse_help_params(char* s)
{
    if      (!strcmp(s, "l"))         o_help_layouts();
    else if (!strcmp(s, "lay"))       o_help_layouts();
    else if (!strcmp(s, "layout"))    o_help_layouts();
    else if (!strcmp(s, "layouts"))   o_help_layouts();

    else if (!strcmp(s, "dif"))            o_help_difficulty();
    else if (!strcmp(s, "difficulty"))     o_help_difficulty();
    else if (!strcmp(s, "difficulties"))   o_help_difficulty();
    else if (!strcmp(s, "lev"))            o_help_difficulty();
    else if (!strcmp(s, "level"))          o_help_difficulty();
    else if (!strcmp(s, "levels"))         o_help_difficulty();

    else if (!strcmp(s, "c")   ||  !strcmp(s, "colemak"))         o_help_colemak();
    else if (!strcmp(s, "cdh") ||  !strcmp(s, "colemak-dh"))      o_help_colemak_dh();
    else if (!strcmp(s, "d")   ||  !strcmp(s, "dvorak"))          o_help_dvorak();
    else if (!strcmp(s, "dl")  ||  !strcmp(s, "dvorak-l"))        o_help_dvorak_l();
    else if (!strcmp(s, "dr")  ||  !strcmp(s, "dvorak-r"))        o_help_dvorak_r();
    else if (!strcmp(s, "t")   ||  !strcmp(s, "tarmak"))          o_help_tarmak_1();
    else if (!strcmp(s, "t1")  ||  !strcmp(s, "tarmak-1"))        o_help_tarmak_1();
    else if (!strcmp(s, "t2")  ||  !strcmp(s, "tarmak-2"))        o_help_tarmak_2();
    else if (!strcmp(s, "t3")  ||  !strcmp(s, "tarmak-3"))        o_help_tarmak_3();
    else if (!strcmp(s, "t4")  ||  !strcmp(s, "tarmak-4"))        o_help_tarmak_4();
}

void o_version(void)
{
    printf("Typingtrainer %s\n", VERSION);
}
