// https://en.wikipedia.org/wiki/Words_per_minute
// https://www.opensourceshakespeare.org/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#include <ncurses.h>

#include "dictionary.h"
#include "settings.h"
#include "display.h"
#include "io.h"
#include "data.h"
#include "keyboard.h"
#include "timer.h"

// TODO Tarmak-dh 1-4.


void init_ncurses(void)
{
    setlocale(LC_ALL, "");
    initscr();
    cbreak();
    noecho();
    nonl();
    timeout(-1);
    intrflush(stdscr, 0);
    keypad(stdscr, 1);
    curs_set(0);

    if (!has_colors()) exit(1);
    if (start_color() != OK) exit(1);

    init_pair(1, COLOR_WHITE, COLOR_BLACK);     // Normal
    init_pair(2, COLOR_BLACK, COLOR_WHITE);     // Normal highlit
    init_pair(3, COLOR_GREEN, COLOR_BLACK);     // Home row
    init_pair(4, COLOR_BLACK, COLOR_GREEN);     // Home row highlit
    init_pair(5, COLOR_BLACK, COLOR_GREEN);     // Home row highlit
    init_pair(6, COLOR_WHITE, COLOR_RED);       // Incorrect

    init_pair(7, COLOR_RED, COLOR_BLACK);       // Red result
    init_pair(8, COLOR_YELLOW, COLOR_BLACK);    // Yellow result
    init_pair(9, COLOR_GREEN, COLOR_BLACK);     // Green result
    atexit(quit);
}

int main(int argc, char** argv)
{
    settings* s = settings_init();
    dictionary* dict = dict_init();

    if (parse_parameters(argc, argv, s)) {

        data data = {0, 0, 0, 0};
        dict_readfile(dict, s, &data, NULL);
        init_ncurses();
        display* disp = display_init_create(6, 70);
        display* kbd = NULL;
        keyboard* kb = NULL;
        timer* t = timer_init();

        if (s->kb) {
            kb = kb_init(s);
            kbd = display_init_create_kb(kb->totheight, kb->totwidth, 10);
            sneedville(kb, kbd);
        }

        update_text(dict, s, disp, &data);
        while (data.curword < s->numwords) {
            read_input(dict, s, &data, disp, kbd, kb, t);
            update_text(dict, s, disp, &data);
        }
        display_result(disp, &data, kbd, t);

        display_free(disp);
        free(disp);
        display_free(kbd);
        free(kbd);
        kb_free(kb);
        free(kb);
        free(t);
    }

    dict_free(dict);
    free(s);
    free(dict);
	return 0;
}
