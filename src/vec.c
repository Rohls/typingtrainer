#include "vec.h"

#include <stdlib.h>
#include <string.h>

// Push char {{{

void vec_push_c(Vector* v, char c)
{
    if (v->len + 1 >= v->cap) {
        v->cap += 2;
        char* new = (char*)realloc(v->c, v->cap);
        v->c = new;
    }

    v->c[v->len] = c;
    ++v->len;
    v->c[v->len] = '\0';
}

// }}}

// Push string {{{

void vec_push_str(Vector* v, const char* s, int len)
{
    if (v->len + len + 1 >= v->cap - 1) {
        v->cap += (len * 2);
        char* new = (char*)realloc(v->c, v->cap);
        /* if (!new) return; */
        memcpy(&new[v->len], s, len);
        v->len += len;
        new[v->len] = '\0';
        v->c = new;
    }
    else {
        memcpy(&v->c[v->len], s, len);
        v->len += len;
        v->c[v->len] = '\0';
    }
}

// }}}

// Reset/Empty {{{

void vec_reset(Vector* v)
{
    char* new = (char*)realloc(v->c, 2);
    v->cap = 2;
    v->len = 0;
    v->c = new;
    memset(v->c, '\0', v->cap);
}

// }}}

// Free {{{

void vec_free(Vector* v)
{
    if (v->c) free(v->c);
}

// }}}

// Pop char {{{

void vec_pop_c(Vector* v)
{
    if (v->len == 0) return;
    if (!v->c) return;
    v->c[v->len] = '\0';
    --v->len;
}

// }}}

// Is equal {{{

int vec_is_equal(Vector* v1, Vector* v2)
{
    if (v1 == NULL || v2 == NULL) return -1;
    if (v1->c == NULL || v2->c == NULL) return -1;
    if (v1->len != v2->len) return 0;
    for (int i = 0; i < v1->len; i++) {
        if (v1->c[i] != v2->c[i]) return 0;
    }
    return 1;
}

// }}}

// str equal {{{

int vec_str_equal(Vector* v, const char* s)
{
    if (v == NULL || v->c == NULL || s == NULL) return 0;
    int slen = strlen(s);
    if (v->len != slen) return 0;
    for (int i = 0; i < slen; i++) {
        if (v->c[i] != s[i]) return 0;
    }
    return 1;
}

// }}}
