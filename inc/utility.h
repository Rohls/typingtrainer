#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

enum layouts {
    QWERTY = 0,
    COLEMAK,
    COLEMAK_DH,
    DVORAK,
    DVORAK_L,
    DVORAK_R,
    TARMAK_1,
    TARMAK_2,
    TARMAK_3,
    TARMAK_4
};

void quit(void);
void die(const char* msg);
char* getlayout(int layout, int level, int uppercase);

#endif
