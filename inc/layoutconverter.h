#ifdef _LAYOUT_CONVERTER_H_
#define _LAYOUT_CONVERTER_H_

int convert_dvorak(int c);
int convert_dvorak_l(int c);
int convert_dvorak_r(int c);
int convert_colemak(int c);
int convert_colemak_dh(int c);
int convert_tarmak_1(int c);
int convert_tarmak_2(int c);
int convert_tarmak_3(int c);
int convert_tarmak_4(int c);

#endif
