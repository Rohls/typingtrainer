#include "utility.h"
#include "io.h"
#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#define VERSION "1.0.3"

typedef struct settings {
    int layout;
    int level;
    int numwords;
    int random;
    int kb;
    int kb_intr;
    int sentencelen;
    int sentenceheight;
    int interactive;
    int entirefile;
    int textfile;
} settings;

settings* settings_init(void);

int setlayout(settings* s, char* str);

int isnum(char* s);

int parse_parameters(int argc, char** argv, settings* s);

#endif
