#ifndef _TIMER_H_
#define _TIMER_H_

#include <time.h>
#include <stdlib.h>

#include "utility.h"

typedef struct timer {
    time_t start;
    time_t finish;
    time_t time;
    unsigned int hours;
    unsigned int minutes;
    unsigned int seconds;
} timer;

timer* timer_init(void);

void timer_start(timer* t);

void timer_stop(timer* t);

void timer_calc_time(timer* t);

#endif
