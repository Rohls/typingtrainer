#include "settings.h"
#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <stdlib.h>
#include <ncurses.h>

#include "utility.h"
#include "display.h"


typedef struct key {
    char c;
    int posy;
    int posx;
    int h;
    int w;
    int is_hr;
} key;

typedef struct keyboard {
    key* keys;
    int nkeys;
    int numrow;
    int spacing;
    char* hrkeys;
    char* layout;
    key* last_hl;
    int totwidth;
    int totheight;
} keyboard;

void kb_free(keyboard* kb);

void k_draw(display* disp, key* k);

void k_draw_hl(display* disp, key* k);

void kb_clear_hl(keyboard* kb, display* disp);

void kb_get_layout(keyboard* kb, int layout, int level);

int is_hrkey(keyboard* kb, char c);

int kb_create(keyboard* kb);

keyboard* kb_init(settings* s);

void kb_highlight_key(keyboard* kb, display* disp, int c);

void sneedville(keyboard* kb, display* disp);

#endif
