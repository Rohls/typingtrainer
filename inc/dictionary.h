#include "utility.h"
#include "settings.h"
#include "data.h"
#ifndef _DICTIONARY_H_
#define _DICTIONARY_H_

#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>


typedef struct dictionary dictionary;

struct dictionary {
    char** data;
    size_t len;
    size_t cap;
};

int iskey(const char* keys, int c);

dictionary* dict_init(void);

void pushstr(dictionary* d, char* s, size_t len);

void dict_free(dictionary* d);

void dict_randomize(dictionary* dict, settings* s);

int romeojuliet(dictionary* dict, settings* s);

int readfile(dictionary* dict, settings* s, const char* fpath, const char* keys);

void dict_readfile(dictionary* d, settings* s, data* data, const char* fpath);

#endif
