#ifndef _IO_H_
#define _IO_H_

#include <ncurses.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "dictionary.h"
#include "settings.h"
#include "display.h"
#include "data.h"
#include "layoutconverter.h"
#include "utility.h"
#include "vec.h"
#include "keyboard.h"
#include "timer.h"

#define QUIT_TIMES 2

enum colors {
    CLEAR,
    CORRECT,
    WRONG = 6
};

void toggleattr(WINDOW* win, int color);

void charadd(WINDOW* win, int y, int x, char c, int attrcolor);

int is_useless(int c);

void read_input(dictionary* dict, settings* s, data* d, display* disp, display* kbd, keyboard* kb, timer* t);

void update_text(dictionary* dict, settings* s, display* disp, data* d);

void o_help(void);

void o_help_level(void);

void o_help_layouts(void);

void o_help_keyboard(void);

void parse_help_params(char* s);

void o_version(void);

void o_help_colemak(void);
void o_help_colemak_dh(void);
void o_help_dvorak(void);
void o_help_dvorak_l(void);
void o_help_dvorak_r(void);
void o_help_tarmak_1(void);
void o_help_tarmak_2(void);
void o_help_tarmak_3(void);
void o_help_tarmak_4(void);

#endif
